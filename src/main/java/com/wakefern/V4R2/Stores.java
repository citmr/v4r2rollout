package com.wakefern.V4R2;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Stores {

    public Stores() {
    }

    public static ArrayList<String> getStoresByChain(String chain) {
        ArrayList<String> results = new ArrayList<>();

        Database database = new Database();
        try {
            CallableStatement cs = database.establishConnection().prepareCall("{call [dbo].[sp_STORES_S_By_Chain](?)}");
            cs.setString("chainNumber", chain);

            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                results.add(String.valueOf(rs.getInt("STORE_NUMBER")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return results;
    }

    public static ArrayList<String> getAll() {
        ArrayList<String> results = new ArrayList<>();

        Database database = new Database();
        try {
            CallableStatement cs = database.establishConnection().prepareCall("{call [dbo].[sp_STORE_SelectAll]()}");

            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                results.add(String.valueOf(rs.getInt("STORE_NUMBER")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return results;
    }
}
