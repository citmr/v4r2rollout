package com.wakefern.V4R2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    private static Connection connection;

    public Database() {
    }

    public Connection establishConnection() throws SQLException {
        if (connection == null) {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                connection = DriverManager.getConnection("jdbc:sqlserver://devsqlsrv1.wakefern.com;user=DEVAPP1;password=developmentTest1;database=POSDEV1");
            } catch (ClassNotFoundException var1) {
                System.err.println("Could not find sqljdbc42.jar. " + var1.getMessage());
            }
        }
        return connection;
    }
}

