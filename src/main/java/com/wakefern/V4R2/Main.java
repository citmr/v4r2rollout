package com.wakefern.V4R2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {


    public static void main(String[] args) {

        String svMemberOut = "./svmemberByStore/";
        String svMemberIn = "./svmember/";
        FileBuilder.buildJarsbyChain(svMemberIn,svMemberOut);
        FileBuilder.buildEmptyJars();

    }
}
