package com.wakefern.V4R2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileBuilder {

    public static void buildJarsbyChain(String inputPath, String outputPath) {

        // Get file directory list
        File folder = new File(inputPath);
        File[] listOfFiles = folder.listFiles();

        for (File svJar : listOfFiles) {
            if (svJar.isFile()) {

                String filename = svJar.getName();

                if (!getFileNum(filename).isEmpty()) {

                    String chain = getFileNum(filename);
                    ArrayList<String> stores = Stores.getStoresByChain(chain);
                    for (String store : stores) {
                        String filename1 = filename.substring(0, filename.indexOf("."));
                        String copyName = outputPath + filename1 + "." + store + ".jar";
                        copyFile(svJar, new File(copyName));
                    }
                }
            }
        }
    }

    public static void buildEmptyJars() {

        String svStorePath = "./svStore";
        String svMemberPath = "./svmemberByStore/";
        ArrayList<String> svStores = new ArrayList<>();
        ArrayList<String> svMembers = new ArrayList<>();

//        List of stores with svStore
        File svStoreFolder = new File(svStorePath);
        File[] svStoreFiles = svStoreFolder.listFiles();
        for (File svStore : svStoreFiles) {
            svStores.add(getFileNum(svStore.getName()));
        }

//        List of stores with svMember
        File svMemberFolder = new File(svMemberPath);
        File[] svMemberFiles = svMemberFolder.listFiles();
        for (File svMember : svMemberFiles) {
            svMembers.add(getFileNum(svMember.getName()));
        }

//        Create empty sv*.jar files
        String emptyJarsPath = "./empty";
        String emptyJarsByStorePath = "./emptyByStore";
        for (String store : Stores.getAll()) {
            if (!svMembers.contains(store)) {
                String svMemberCopy = emptyJarsByStorePath + "/svmember." + store + ".jar";
                File emptySvMember = new File(emptyJarsPath + "/svmember.jar");
                copyFile(emptySvMember, new File(svMemberCopy));
            }

            if (!svStores.contains(store)) {
                String svStoreCopy = emptyJarsByStorePath + "/svstore." + store + ".jar";
                File emptySvStore = new File(emptyJarsPath + "/svstore.jar");
                copyFile(emptySvStore, new File(svStoreCopy));
            }

            String svPoc1Copy = emptyJarsByStorePath + "/svpoc1." + store + ".jar";
            File emptySvPoc1 = new File(emptyJarsPath + "/svpoc1.jar");
            copyFile(emptySvPoc1, new File(svPoc1Copy));

            String svPoc2Copy = emptyJarsByStorePath + "/svpoc2." + store + ".jar";
            File emptySvPoc2 = new File(emptyJarsPath + "/svpoc2.jar");
            copyFile(emptySvPoc2, new File(svPoc2Copy));

            String svUserCopy = emptyJarsByStorePath + "/svuser." + store + ".jar";
            File emptySvUser = new File(emptyJarsPath + "/svuser.jar");
            copyFile(emptySvUser, new File(svUserCopy));

        }

    }

    private static String getFileNum(String filename) {

        String result = "";

        String set = "(.*?).jar";
        Pattern pattern = Pattern.compile(set);
        Matcher matcher = pattern.matcher(filename.substring(filename.indexOf(".") + 1));
        if (matcher.find()) {
            result = matcher.group(1);
        }

        return result;
    }

    private static void copyFile(File file1, File file2) {
        try {
            Files.copy(file1.toPath(), file2.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
